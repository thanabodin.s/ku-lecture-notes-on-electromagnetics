
[$`\footnotesize \copyright 2020 \: \text {Denchai Worasawate}`$](https://www.researchgate.net/profile/Denchai-Worasawate)  


|[บทที่ 1](ch1TH.md)| [หน้าหลัก](../../README.md) | [Contents](../contentEN.md) | [สารบัญ](../contentTH.md) |
| ---------- | ---------- | ---------- | -------- |    

|[ก่อนหน้า](ch1-07TH.md)| [ต่อไป](ch1-08TH.md) |
| ---------- | ---------- |    

### การแปลงชื่อของจุดระหว่างพิกัดฉาก $`(x,y,z)`$   กับพิกัดแบบทรงกลม $`(r,\theta,\phi)`$  
ในรูป $`(d)`$ เราจะเห็นว่า  
ถ้าเรารู้ค่า $`(r,\theta,\phi)`$ เราสามารถหาค่า $`(x,y,z)`$ ได้ดังนี้
- $`x = \rho \cos \phi`$ เมื่อ $`\rho  = r \sin \theta`$     
- $`y = \rho \sin \phi`$ เมื่อ $`\rho  = r \sin \theta`$    
- $`x =  r \sin \theta \cos \phi`$  
- $`y =  r \sin \theta \sin \phi`$  
- $`z  = r \cos \theta`$   

ถ้าเรารู้ค่า $`(x,y,z)`$ เราสามารถหาค่า $`(r,\theta,\phi)`$ ได้ดังนี้
- $`\rho = \sqrt{x^2+y^2}`$
- $`r = \sqrt{\rho^2+z^2}`$
- $`r = \sqrt{x^2+y^2+z^2}`$
- $`\theta = \cos^{-1} \frac{z}{r}`$ หรือ $`\theta = \sin^{-1} \frac{\rho}{r}`$
- $`\theta = \cos^{-1} \frac{z}{\sqrt{x^2+y^2+z^2}}`$ หรือ $`\theta = \sin^{-1} \sqrt{\frac{{x^2+y^2}}{{x^2+y^2+z^2}}}`$
- $`\phi =\tan^{-1} \frac {y}{x}`$  

ค่า $`r`$ มีค่ามากว่าหรือเท่าศูนย์เสมอเพราะเป็นระยะห่างจากจุดกำเนิด และค่า $`\phi`$ มีค่าตั้งแต่ศุนย์ถึง $`2 \pi`$ เรเดียน ที่แกน $`x`$ ค่า $`\phi=0`$ ที่แกน $`y`$ ค่า $`\phi=\frac{\pi}{2}`$ ที่แกน $`-x`$ ค่า $`\phi=\pi`$ ที่แกน $`-y`$ ค่า $`\phi=\frac{2 \pi}{2}`$ จะวนกลับไปที่แกน $`x`$ โดยจุดที่เกือบถึงแกน $`x`$ จะมีค่าเกือบถึง $`2 \pi`$ แต่จะไม่ถึง $`2 \pi`$ เพราะจะไปซ้ำกับจุดบนแกน $`x`$ ซึ่งกำหนดไปแล้วว่า $`\phi=0`$ โดยปกติ $`\tan^{-1}`$ จะมีค่าอยู่ในช่วง $`-\frac{\pi}{2}`$ ถึง $`\frac{\pi}{2}`$ ดังนั้นเวลากดเครื่องคิดเลขเราจะต้องนำค่าทีได้มาแปลงเป็นค่าที่อยู่ในช่วงศูนย์ถึง $`2\pi`$ ตามนิยามของ $`\phi`$ เช่น ถ้า $`x=-1`$ และ $`y=-1`$ เรากดเครื่องคิดเลขโดยพิมพ์ $`tan^{-1}\frac{-1}{-1}`$ เราจะได้คำตอบเป็น $`\frac{\pi}{4}`$ แต่จริงๆแล้วจุดอยู่ในควอแดรนต์ที่สาม $`\phi`$ มีค่าเท่ากับ $`\pi \frac{\pi}{4} = \frac{5\pi}{4}`$   
เนื่องจากเราให้ $`\phi \in \left[0, 2\pi \right)`$ ดังนั้นเพื่อให้จุดใดๆมีชื่อเรียกได้แค่หนึ่งชื่อเราจะต้องให้ $`\theta \in \left[0, \pi \right)`$ ถ้าเรายอมให้ $`\theta > \pi`$ จุด $`(r=1,\theta=\frac{\pi}{4},\phi = 0)`$ สามารถเรียกได้อีกชื่อหนึ่งคือ $`(r=1,\theta=\frac{7\pi}{4},\phi = \pi)`$  
ในระบบหน่วย SI หน่วยของ $`\theta`$ และ $`\phi`$ คือ เรเดียน ถ้าเราไม่ได้เขียนหน่วย เราจะคือว่าหน่วยที่ใช้เป็นหน่วย SI ถ้าเราต้องการใช้หน่วยเป็นองศา เราต้องเขียนหน่วยกำกับไว้เสมอ  
สรุปได้ว่าค่าของตัวแปรในพิกัดแบบทรงกลมจะมีค่าดังนี้  
- $`r \in \left[ 0, \infty \right)`$  
- $`\theta \in \left[ 0, \pi \right)`$  
- $`\phi \in \left[ 0, 2\pi \right)`$  

ที่จุด $`P(x=1,y=2,z=3)`$ เราจะได้ว่า 
- $`1 = \rho \cos \phi`$  
- $`2 = \rho \sin \phi`$  
หรือ $`\rho = \sqrt{ 1^2 + 2^2} = \sqrt{ 5}`$ และ $`\phi = \tan \frac{2}{1} = 1.1071`$ ดังนั้นจุด $`P(x=1,y=2,z=3)`$ จะมีชื่อว่า $`P(\rho=\sqrt{5},\phi=1.1071,z=3)`$ ในพิกัดแบบทรงกระบอก และ $`r = \sqrt{\rho^2+z^2} = \sqrt{5+3^2} = \sqrt{14}`$ และ $`\theta = \cos^{-1} \frac{z}{r} = \cos^{-1} \frac{3}{\sqrt{14}} = 0.6405`$ ดังนั้นจุด $`P(x=1,y=2,z=3)`$ จะมีชื่อว่า $`P(r=\sqrt{14},\theta = 0.6405, \phi=1.1071)`$ ในพิกัดแบบทรงกลม 

### ตัวอย่างการประยุกต์ใช้พิกัดแบบทรงกระบอก  
1.7.1 [ตัวอย่างการหาความยาวของเส้นโค้ง](ch1-07-01TH.md)  
1.7.2 [ตัวอย่างการหาพื้นที่ครึ่งวงกลม](ch1-07-02TH.md)  
1.7.3 [ตัวอย่างการหาปริมาตรครึ่งทรงกลม](ch1-07-03TH.md)  

|[ก่อนหน้า](ch1-07TH.md)| [ต่อไป](ch1-08TH.md) |
| ---------- | ---------- |    

<table align="center">
<tr><td align="center" width="9999">
<img src="./asset/sphere_ab.svg" align="center" width=50% alt="" />
</td></tr>
</table>  

(a) แสดงพื้นผิวที่ตัวแปรในพิกัดแบบทรงกระบอกมีค่าคงที่ (b) แสดงทิศทางของแกน $`\rho`$ แกน $`\phi`$ และ แกน $`z`$ ที่จุด P (ดัดแปลงจาก [Hayt 2011](#Hayt))  

<table align="center">
<tr><td align="center" width="9999">
<img src="./asset/sphere_c.svg" align="center" width=50% alt="" />
</td></tr>
</table>  

(c) แสดงความยาวดิฟเฟอเรนเชียล (differential length) ปริมาตร ดิฟเฟอเรนเชียล (differential volume) พื้นผิวดิฟเฟอเรนเชียล (differential surface) (ดัดแปลงจาก [Hayt 2011](#Hayt))

<table align="center">
<tr><td align="center" width="9999">
<img src="./asset/sphere_cartesian.svg" align="center" width=50% alt="" />
</td></tr>
</table>  

(d) แสดงการแปลงชื่อของจุดระหว่างพิกัดฉากกับพิกัดแบบทรงกระบอก (differential surface) (ดัดแปลงจาก [Hayt 2011](#Hayt))

อ้างอิง  
- <a name="Hayt"></a> Hayt, W. H., & Buck, J. A. (2011). Engineering Electromagnetics (8th ed.). McGraw-Hill Professional.

|[ก่อนหน้า](ch1-07TH.md)| [ต่อไป](ch1-08TH.md) |
| ---------- | ---------- |    


[$`\footnotesize \copyright 2020 \: \text {Denchai Worasawate}`$](https://www.researchgate.net/profile/Denchai-Worasawate)  


