
[$`\footnotesize \copyright 2020 \: \text {Denchai Worasawate}`$](https://www.researchgate.net/profile/Denchai-Worasawate)  


|[บทที่ 1](ch1TH.md)| [หน้าหลัก](../../README.md) | [Contents](../contentEN.md) | [สารบัญ](../contentTH.md) |
| ---------- | ---------- | ---------- | -------- |   

|[ก่อนหน้า](ch1-07-01TH.md)| [ต่อไป](ch1-07-03TH.md) |
| ---------- | ---------- |    

### 1.7.2 [ตัวอย่างการหาพื้นที่ครึ่งวงกลม](ch1-07-02TH.md)  
จงหาพื้นที่ของครึ่งวงกลมที่มีรัศมีเท่ากับ $`a`$  
#### วิธีทำ  
สมการวงกลมที่มีจุดศูนย์กลางอยู่ที่จุดกำเนิดและมีรัศมีเท่ากับ $`a`$ คือ $`x^2+y^2=a^2`$ หรือเขียนในพิกัดทรงกระบอกโดยแทนค่า $`r^2 = x^2+y^2+z^2`$ ได้ดังนี้ ได้ดังนี้ $`r^2=a^2`$ และ $`z=0`$ หรือ $`r=a`$ และ $`\theta = \frac{\pi}{2}`$  
  
เราให้พื้นที่ดิฟเฟอเรนเชียล หรือพื้นที่เล็กที่สุดโดยแบ่งในแนว $`r`$ กับ $`\phi`$ เราจะได้ว่า $`ds = r \sin \theta d r d\phi`$ เมื่อ $`\theta = \frac{\pi}{2}`$ เราจะได้ $`ds = r  d r d\phi`$  
เนื่องจากพื้นที่อยู่บนระนาบ $`xy`$ จึงได้สมการอินทิเกรตคล้ายกับการหาด้วย[พิกัดทรงกระบอก](ch1-06-03TH.md) เพียงเปลี่ยนตัวแปร $`\rho`$ เป็น $`r`$ เพราะ $`\rho= r \sin \theta`$ และบนระนาบ $`xy`$ $`\theta = \frac{\pi}{2}`$ จะได้ว่า $`\rho= r `$ ดัวนั้นวิธีทำด้านล่างจะเหมือนกับการหาด้วย[พิกัดทรงกระบอก](ch1-06-03TH.md) เพียงเปลี่ยนตัวแปร $`\rho`$ เป็น $`r`$ เท่านั้น   
        
ถ้าเราแบ่งพื้นที่ของครึ่งวงกลมเป็นสี่เหลี่ยมชิ้นเล็กๆ โดยมีการขนาดการเปลี่ยนแปลง $`\Delta r`$ เท่ากันทุกชิ้น และ ขนาดการเปลี่ยนแปลง $`\Delta \phi`$ เท่ากันทุกชิ้น ขนาดของสี่เหลี่ยมชิ้นเล็กๆเท่ากับ $`\Delta s_k = r_i \Delta r_i \Delta \phi_j`$ แล้วเอาพื้นที่ของสี่เหลี่ยมแต่ละชิ้นมาร่วมกันทั้งหมด เราจะได้พื้นที่ของครึ่งวงกลมโดยประมาณ 
```math
S \approx \sum_{i=1}^{i=N_{r}} \sum_{j=1}^{j= N_{\phi}} { r_i \Delta \phi_j \Delta r_i}
```  
```math
S \approx \sum_{i=1}^{i=N_{r}} \sum_{j=1}^{j= N_{\phi}} {  \Delta \phi_j r_i \Delta r_i}
```  
```math
S \approx \sum_{i=1}^{i=N_{r}} \left( N_{\phi} \Delta \phi \right) r_i \Delta r_i
```  
$` \left( N_{\phi} \Delta \phi \right)`$ มีค่าเท่ากับ $`\pi`$ เพราะเราคิดแค่**ครึ่งวงกลม**  
```math
S \approx \sum_{i=1}^{i=N_{r}}  \pi r_i \Delta r_i
```  
```math
S \approx \pi\Delta r \sum_{i=1}^{i=N_{r}}   r_i 
```  
$`r_i`$ คือจุดกึ่งกลางของแต่ละชิ้นดังนั้น $`r_i = \Delta r \left(i-0.5\right)`$ เราจึงได้สมการเป็น   
```math
S \approx \pi\Delta r \sum_{i=1}^{i=N_{r}}  \Delta r \left(i-0.5\right)
```  
```math
S \approx \pi\Delta r^2 \sum_{i=1}^{i=N_{r}}   \left(i-0.5\right)
```  
```math
S \approx \pi\Delta r^2 \left(\frac {N_{r}}{2} \left( N_{r}+1\right)-0.5 N_{r} \right)
```  
```math
S \approx \pi\Delta r \left(N_{r} \Delta r \right) \left(\frac {1}{2} \left( N_{r}+1\right)-0.5  \right)
```  
```math
S \approx \pi\Delta r \left(N_{r} \Delta r \right) \left(\frac { N_{r}}{2}   \right)
```  
```math
S \approx \pi \left(N_{r} \Delta r \right)^2 \frac{1}{2}
```  
$`N_{r} \Delta r`$ มีค่าเท่ากับรัศมีของวงกลมคือ $`a`$ ดังนี้
```math
S \approx \frac{\pi a^2}{2}
```  
เนื่องจากเราประมาณ $`r_i \Delta \phi_j \Delta r_i`$ เป็นพื้นที่ของสี่เหลี่ยมมุมฉากที่มีด้านเท่ากับ $`r_i  \Delta r_i`$ และ $` \Delta r_i`$ ซึ่งมีค่าเท่ากับพื้นที่ของสี่เหลี่ยมที่มีด้านเป็นส่วนโค้งของวงกลมพอดี ดังนั้นค่าประมาณจึงเท่ากับค่าจริงพอดีแต่ค่าประมาณไม่จำเป็นต้องเท่ากับค่าจริงเสมอไป ซึ่งถ้าเราให้ค่า  $` \Delta r`$ และ  $` \Delta \phi`$ มีขนาดเล็กลงจะทำให้ค่าประมาณใกล้เคียงกับค่าจริงมากขึ้น ถ้าเราให้ค่าลู่เข้าศูนย์เราจะได้ค่าจริง แต่เราจะมีจำนวนพื้นที่เล็กๆเป็นอนันต์ 
ซึ่งเราไม่สามารถเอาค่ามารวมกันเป็นอนันต์(infinite) ครั้ง แต่เราจะเปลี่ยนเครื่องหมาย summation เป็นเครื่องหมาย integrate แทนและเปลี่ยน $`{\Delta r_i}`$ เป็น $`{dr}`$ และ $`{\Delta \phi_j}`$ เป็น $`{d\phi}`$ ดังนั้น 
```math
S = \sum_{i=1}^{i=\infty} \sum_{j=\phi_{start(i)}}^{j= \phi_{stop(i)}} { r_i \Delta \phi_j \Delta r_i} = \int_{r=initial}^{r = final} \int_{\phi=g(r)}^{\phi = f(r)} r d\phi dr
```  
โดย $`initial`$ เป็นตำแหน่ง $`i=1`$ และ $`final`$ เป็นตำแหน่ง $`i=\infty`$ และค่าของ $`f(r)`$ และ $`g(r)`$ ขึ้นอยู่กับค่าของ $`r`$  
จากสมการวงกลมเราจะได้ $`g(r) = 0`$ และ $`f(r) = \pi`$ และ $`initial = 0 `$ และ $`final = a`$ เราจะได้สมการปริพันธ์ดังนี้  
```math
S = \int_{r=0}^{r= a} \int_{\phi=0}^{\phi = \pi} r d\phi dr \newline  
S = \int_{r=0}^{r= a} \phi \bigg|_0^{\pi} r dr \newline
S = \int_{r=0}^{r= a} \pi r dr \newline
S = \pi \frac{r^2}{2}\bigg|_{r=0}^{r= a} \newline
S =  \frac{\pi a^2}{2} \newline
```  
ถ้าเรากลับไปดูตัวอย่างที่คำนวนด้วยพิกัดฉากเราจะเห็นว่าเราได้คำตอบเหมือนกันแต่ สมการอินทิเกรตยากกว่ามาก เราจึงสมควรพิจารณาปัญหาก่อนว่า ปัญหามีลักษณะคล้ายกับพิดแบบใด ถ้าเราเลือกใช้พิกัดได้ถูกกับปัญหา เราก็จะสามารถแก้ปัญหาได้ง่ายขึ้น   


|[ก่อนหน้า](ch1-07-01TH.md)| [ต่อไป](ch1-07-03TH.md) |
| ---------- | ---------- |    

[$`\footnotesize \copyright 2020 \: \text {Denchai Worasawate}`$](https://www.researchgate.net/profile/Denchai-Worasawate)  




