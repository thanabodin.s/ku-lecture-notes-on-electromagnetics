
|[บทที่ 1](ch1TH.md)| [หน้าหลัก](../../README.md) | [Contents](../contentEN.md) | [สารบัญ](../contentTH.md) |
| ---------- | ---------- | ---------- | -------- |   

|[ก่อนหน้า](ch1-06-02TH.md)| [ต่อไป](ch1-07TH.md) |
| ---------- | ---------- |    

### 1.6.3 [ตัวอย่างการหาปริมาตรครึ่งทรงกลม](ch1-06-03TH.md)  
จงหาปริมาตรของครึ่งทรงกลมที่มีรัศมีเท่ากับ $`a`$  
#### วิธีทำ  
สมการพื้นผิวทรงกลมที่มีจุดศูนย์กลางอยู่ที่จุดกำเนิดและมีรัศมีเท่ากับ $`a`$ คือ $`x^2+y^2+z^2=a^2`$ หรือเขียนในพิกัดทรงกระบอกโดยแทนค่า $`\rho^2 = x^2+y^2`$ ได้ดังนี้ $`\rho^2+z^2=a^2`$ หรือ พื้นผิวครึ่งทรงกลมคือ $`z= \sqrt{a^2-\rho^2}`$

เราให้ปริมาตรดิฟเฟอเรนเชียล หรือปริมาตรที่เล็กที่สุด $`dv = \rho d\rho d\phi dz`$  
ถ้าเราแบ่งปริมาตรของครึ่งทรงกลมเป็นกล่องสี่เหลี่ยมเล็กๆ โดยให้ขนาดของกล่องเท่ากับ $`\Delta v_m = \rho_i \Delta \rho_i \Delta \phi_j \Delta z_k`$ ซึ่งถ้าเราให้ขนาดของ $`\Delta \rho_i`$ $`\Delta \phi_j`$ และ $`\Delta z_k`$ ของแต่ละกล่องมีขนาดเท่ากัน และ $`\rho_i`$ คือระยะจากแกน $`z`$ ไปยังศูนย์กลางของกล่อง แล้วเอาปริมาตรของกล่องสี่เหลี่ยมแต่ละชิ้นมาร่วมกันทั้งหมด เราจะได้ปริมาตรของครึ่งทรงกลมโดยประมาณ
```math
V \approx \sum_{m=1}^{N} {\Delta v_m} = \sum_{i=1}^{i=N_\rho} \sum_{j=\phi_{start(i)}}^{j= phi_{stop(i)}}  \sum_{k=z_{start(i,j)}}^{k= z_{stop(i,j)}} { \Delta z_k \Delta \phi_j \rho_i \Delta \rho_i}
```   
ค่าของ $`\phi_{start}`$ และ $`\phi_{stop}`$ อาจขึ้นอยู่กับค่า $`i`$ แต่ข้อนี้มุม $`\phi`$ ไม่ขึ้นกับค่า $`\rho`$ แต่เป็นวงกลมคือ $`\phi_{start}=0`$ และ $`\phi_{stop}= 2\pi`$ และ $`z_{start}`$ และ $`z_{stop}`$ อาจขึ้นอยู่กับค่า $`i,j`$ โดยที่ $`z_{start}`$ จะเป็นระนาบ $`xy`$ $`z=0`$ และ $`z_{stop}`$ จะเป็นพื้นผิวทรงกลม $`z= \sqrt{a^2-\rho_i^2}`$ เราจะได้  
```math
V \approx \sum_{m=1}^{N} {\Delta v_m} = \sum_{i=1}^{i=N_\rho} \sum_{j=(\phi=0)}^{j= (\phi = 2\pi)}\sum_{k=(z=0)}^{k= \left(z=\sqrt{a^2-\rho_i^2}\right)}  { \Delta z_k \Delta \phi_j \rho_i \Delta \rho_i}  

```   
ที่ $`\rho = \rho_i`$ ค่าของ
$`\sum_{k=(z=0)}^{k= \left(z=\sqrt{a^2-\rho_i^2}\right)}  { \Delta z_k} = \sqrt{a^2-\rho_i^2}`$ เพราะคือการบวกระยะ $`\Delta z`$ ที่เริ่มจากชิ้นที่ $`z=0`$ ถึงชิ้นที่ $`z= \sqrt{a^2-\rho_i^2}`$ และ $`\sum_{j=(\phi=0)}^{j= (\phi = 2\pi)}  \Delta \phi_j = 2\pi`$ เพราะคือการบวกค่ามุม $`\Delta \phi`$ ที่เริ่มจากชิ้นที่ $`\phi=0`$ ถึงชิ้นที่ $`\phi= 2\pi`$ เราจะได้   
```math
V \approx 2 \pi \sum_{i=1}^{i=N_\rho} \sqrt{a^2-\rho_i^2}  { \rho_i \Delta \rho_i}  

```   
ถ้าให้ขนาดของ $`\Delta \rho_i`$ เท่ากันทุกๆค่า $`i`$ เป็น $`\Delta \rho`$ และให้ $`N_\rho = 4`$ เราจะได้ $`\Delta \rho = \frac{a}{4}`$ หรือ $`a= 4 \Delta \rho`$ และ $`\rho_i = (i-0.5) \times \Delta \rho`$ แทนค่าลงในสมการเราจะได้  
```math
V \approx 2 \pi \sum_{i=1}^{i=N_\rho} \sqrt{(4 \Delta \rho)^2-((i-0.5) \times \Delta \rho)^2}  { (i-0.5) \times \Delta \rho \Delta \rho_i}  
```   
```math
V \approx 2 \pi \sum_{i=1}^{i=N_\rho} \sqrt{4^2-(i-0.5)^2}  { (i-0.5)  \Delta \rho^3}  
```   
```math
V \approx 2 \pi \sum_{i=1}^{i=4} (i-0.5) \sqrt{16-(i-0.5)^2}  \frac{a^3}{4^3}  
```   
```math
V \approx \frac{\pi a^3}{32}   \sum_{i=1}^{i=4} (i-0.5) \sqrt{16-(i-0.5)^2}  
```   
เราสามารถหาค่า $`\sum_{i=1}^{i=4} (i-0.5) \sqrt{16-(i-0.5)^2} `$ ได้จากเว็บไซต์ [octave-online](https://octave-online.net/) โดยพิมพ์คำสั่งตามด้านล่างซึ่งจะได้คำตอบ $`\sum_{i=1}^{i=4} (i-0.5) \sqrt{16-(i-0.5)^2} = 22.13`$  
```
octave:1> ii = [1 2 3 4]
ii =

   1   2   3   4

octave:2> tmp = (ii-0.5).*sqrt(16-(ii-0.5).^2)
tmp =

   1.9843   5.5621   7.8062   6.7777

octave:3> sum(tmp)
ans = 22.130
```  
เราจะได้ค่าประมาณของปริมาตรคือ  
```math
V \approx \frac{\pi a^3}{32} \times 22.13  
```   
```math
V \approx \frac{22.13}{32}\pi a^3   
```   
```math
V \approx 0.69 \pi a^3   
```   
ค่าจริงของปริมาตรคือ $`\frac{2}{3} \pi a^3`$ หรือ $`0.67 \pi a^3`$ ซึ่งมีความผิดพลาดประมาณ 3% 
ปริมาตรที่ได้จากการประมาณจะต่างจากค่าจริงเพราะกล่องสี่เหลี่ยมไม่สามารถเก็บรายละเอียดของผิวโค้งได้ ผิวโค้งจะมีขรุขระเป็นขั้นบันได ถ้าจะให้ได้ค่าปริมาตรที่ใกล้เคียงกับค่าจริงมากขึ้นเราต้องแบ่ง $`\Delta \rho`$ $`\Delta \phi`$ และ $`\Delta z`$ ให้มีขนาดที่เล็กลง ถ้า $`\Delta \rho`$ $`\Delta \phi`$ และ $`\Delta z`$ มีขนาดที่เล็กที่สุดที่จะเล็กได้ เราจะได้ขนาดของปริมาตรจริง แต่เราต้องรวมปริมาตรของกล่อมซึ่งมีจำนวนเป็นอนั้นต์   
```math
V = \sum_{m=1}^{m= \infty} {\Delta v_m} = \sum_{i=1}^{i=\infty} \sum_{j=(\phi=0)}^{j= (\phi = 2\pi)}\sum_{k=(z=0)}^{k= \left(z=\sqrt{a^2-\rho_i^2}\right)}  { \Delta z_k \Delta \phi_j \rho_i \Delta \rho_i} 
```  
ซึ่งเราไม่สามารถเอาค่ามารวมกันเป็นอนันต์ (infinite) ครั้ง แต่เราจะเปลี่ยนเครื่องหมาย summation เป็นเครื่องหมาย integrate แทนและเปลี่ยน $`{\Delta \rho_i}`$ เป็น $`{d\rho}`$ และ $`{\Delta \phi_j}`$ เป็น $`{d\phi}`$ และ $`{\Delta z_k}`$ เป็น $`{dz}`$ ดังนั้น 
```math
V = \sum_{i=1}^{i=\infty} \sum_{j=(\phi=0)}^{j= (\phi = 2\pi)}\sum_{k=(z=0)}^{k= \left(z=\sqrt{a^2-\rho_i^2}\right)}  { \Delta z_k \Delta \phi_j \rho_i \Delta \rho_i} 
= \int_{\rho=initial}^{\rho= final} \int_{\phi=g_\phi(\phi)}^{\phi = f_\phi(\rho)} \int_{z=g_z(\rho,\phi)}^{z = f_z(\rho,\phi)} \rho dz d\phi d\rho
```  
โดย $`initial`$ เป็นตำแหน่ง $`i=1`$ และ $`final`$ เป็นตำแหน่ง $`i=\infty`$ และค่าของ $`g_\phi(\rho)`$ และ $`f_\phi(\rho)`$ เป็นค่าเริ่มต้นและสิ้นสุดของ $`\phi`$ ตามลำดับซึ่งอาจขึ้นอยู่กับค่าของ $`\rho`$ ส่วนค่าของ $`g_z(\rho,\phi)`$ และ $`f_z(\rho,\phi)`$ เป็นค่าเริ่มต้นและสิ้นสุดของ $`z`$ ตามลำดับซึ่งอาจขึ้นอยู่กับค่าของ $`\rho`$ และ $`\phi`$  
จากรูปทรงครึ่งทรงกลมเราจะได้ว่าที่ตำแหน่ง $`(\rho,\phi)`$ ใดๆค่า $`z`$ จะเปลี่ยนแปลงระหว่างระนาบ $`xy`$, $`z=0`$  และ $`z_{stop}`$ จะเป็นพื้นผิวทรงกลม $`z = \sqrt{a^2-\rho^2}`$ หรือ $`g_z(x,y) = 0 `$ และ $`f_z(x,y) = \sqrt{a^2-\rho^2}`$  และค่า $`\phi`$ จะไม่เปลี่ยนแปลงตาม $`\rho`$ แต่จะเปลี่ยนแปลงเป็นวงกลม $`g_\phi(\rho) = 0`$ และ $`f_\phi(\rho) = 2 \pi`$  
เราจะได้สมการปริพันธ์ดังนี้  
```math
V = \int_{\rho=0}^{\rho= a} \int_{\phi=0}^{\phi = 2 \pi} \int_{z=0}^{z = \sqrt{a^2-\rho^2}} dz d\phi \rho d\rho \newline  
V = \int_{\rho=0}^{\rho= a} \int_{\phi=0}^{\phi = 2 \pi} \sqrt{a^2-\rho^2} d\phi \rho d\rho \newline  
V =  \int_{\phi=0}^{\phi = 2 \pi} d\phi \int_{\rho=0}^{\rho= a}  \rho\sqrt{a^2-\rho^2}  d\rho \newline  
V =  2 \pi \int_{\rho=0}^{\rho= a}  \rho\sqrt{a^2-\rho^2}  d\rho \newline  
```

จาก [integral-calculator.com](https://www.integral-calculator.com/) $`\int_{\rho=0}^{\rho= a}  \rho\sqrt{a^2-\rho^2}  d\rho = \frac{a^3}{3}`$   เราจึงได้ว่าปริมาตรของ**ครึ่ง**ทรงกลมที่มีรัศมีเท่ากับ $`a`$ คือ 
```math
V =  2 \pi \int_{\rho=0}^{\rho= a}  \rho\sqrt{a^2-\rho^2}  d\rho \newline  
V = 2 \pi  \frac{a^3}{3} \newline  
V = \frac{2}{3} \pi a^3\newline
```   
ตรงตามที่เราเรียนมา จะเห็นว่าสมการอินทิเกรตยังยากอยู่แต่อาจดูง่ายกว่าการหาค่าโดยใช้พิกัดฉาก ในหัวข้อถัดไปเราจะพูดถึงพิกัดทรงกลมและเราจะแสดงให้เห็นว่าถ้าเราใช้พิกัดทรงกลมในการหาค่าปริมาตรของทรงกลม สมการอิมทิเกรตจะดูง่ายมาก

|[ก่อนหน้า](ch1-06-02TH.md)| [ต่อไป](ch1-07TH.md) |
| ---------- | ---------- | 
