ในบทที่ 2 เราเรียนเรื่องแรงระหว่างประจุไฟฟ้าและเรารู้ว่าประจุไฟฟ้าเกิดจากอนุภาคสองชนิดคืออิเล็กตรอน (electron) ซึ่งมีประจุลบ และโปรตอน (proton) ซึ่งมีประจุบวก ทั้งสองอนุภาคมีขนาดและมวลซึ่งตาม[กฎความโน้มถ่วงสากลของนิวตัน](https://th.wikipedia.org/wiki/กฎความโน้มถ่วงสากลของนิวตัน) ([Newton's law of universal gravitation](https://en.wikipedia.org/wiki/Newton%27s_law_of_universal_gravitation))   จะมีแรงโน้มระหว่างอนุภาคด้วย ใน Python notebook ด้านล่างแสดงการเปรียบเทียบแรงทางไฟฟ้ากับแรงโน้มถ่วง  
Compare electrical force and gravitational force between 2 particles (electron or proton).  
- [Python notebook](./asset/coulomb-law-examples.ipynb)  
- Python notebook ใน Kaggle: [https://www.kaggle.com/denchai/coulomb-law-examples](https://electromagneticsxchannel.page.link/DQbR)  
  
